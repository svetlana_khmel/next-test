// import React from 'react'
// import Link from 'next/link'
//
// export default () => (
// <ul>
// <li><Link href='/b' as='/a'><a>a</a></Link></li>
// <li><Link href='/a' as='/b'><a>b</a></Link></li>
// </ul>
// )

import React from 'react'
import { bindActionCreators } from 'redux'
import { initStore, startClock, addCount, serverRenderClock } from '../store'
import withRedux from 'next-redux-wrapper'
import Page from '../components/Page'

class Counter extends React.Component {
    static getInitialProps ({ store, isServer }) {
        store.dispatch(serverRenderClock(isServer))
        store.dispatch(addCount())

        return { isServer }
    }

    componentDidMount () {
        this.timer = this.props.startClock()
    }

    componentWillUnmount () {
        clearInterval(this.timer)
    }

    render () {
        return (
            <Page title='Index Page' linkTo='/other' />
    )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addCount: bindActionCreators(addCount, dispatch),
        startClock: bindActionCreators(startClock, dispatch)
    }
}

export default withRedux(initStore, null, mapDispatchToProps)(Counter)